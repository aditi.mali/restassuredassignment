package apiTesting;

import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;

public class GoRestAutherizationUsingUserReqAssignment {

	@Test(priority = 0)
	public void testStatusCode() 
	{
		baseURI = "https://gorest.co.in/";
		given().get("public/v2/users").then().statusCode(200);
	}

	@Test(priority = 1)
	public void firstGetMethod() {
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();
		System.out.println(statusCode);
		System.out.println(res.getBody().asString());
	}

	@Test(priority = 2)
	private void assertGetMethod() 
	{
		Response res = RestAssured.get("https://gorest.co.in/public/v2/users");
		int statusCode = res.getStatusCode();
		Assert.assertEquals(200, statusCode);
	}

	@Test(priority = 3)
	public void postOperationTest() 
	{
		JSONObject req = new JSONObject();
		req.put("name", "adi");
		req.put("email", "adititester189@gmail.com");
		req.put("gender", "female");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		ValidatableResponse res = given().log().all().contentType("application/json")
				.header("authorization", "Bearer bc539c3d6c3b49d96e2333a9861fefbd3da867dd9802dbcb053395f2e409f59c")
				.body(req.toJSONString()).when().post("/users").then().statusCode(201);
	}

	@Test(priority = 4)
	public void putOperationTest()
	{
		JSONObject req = new JSONObject();
		req.put("name", "adi");
		req.put("email", "adititester189@gmail.com");
		req.put("gender", "female");
		req.put("status", "Active");
		System.out.println(req);
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer bc539c3d6c3b49d96e2333a9861fefbd3da867dd9802dbcb053395f2e409f59c")
				.body(req.toJSONString()).when().put("/users/157280").then().statusCode(200);
	}

	@Test(priority = 5)
	public void DeleteOperationTest() 
	{
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer bc539c3d6c3b49d96e2333a9861fefbd3da867dd9802dbcb053395f2e409f59c")
				.when().delete("/users/157280").then().statusCode(204);
	}

}
