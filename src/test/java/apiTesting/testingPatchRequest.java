package apiTesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class testingPatchRequest {
	  @Test
	  public void updatingDtatUsingPatch() 
	  {
		  JSONObject request =new JSONObject();
		  request.put("name", "aditi");
		  request.put("job", "tester");
		  
		  given().body(request.toJSONString())
		  .patch("https://reqres.in/api/users/2")
		  .then()
		  .statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:50:12.293Z"));
		  
		  
	  }
}
