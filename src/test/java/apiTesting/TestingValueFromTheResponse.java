package apiTesting;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class TestingValueFromTheResponse {
	
	
	@Test
	public void testingValue()
	{
		baseURI="https://reqres.in";
	      given().get("/api/users?page=2").then().body("data.first_name", hasItems("Byron"));
	}
	
	@Test
	public void testingMultipleValues()
	{
		baseURI="https://reqres.in";
	      given().get("/api/users?page=2").then().body("data.first_name", hasItems("Byron" , "Michael")).body("data.last_name",hasItem("Lawson"));
	}

}
