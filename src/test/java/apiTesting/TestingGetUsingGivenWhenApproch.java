package apiTesting;
import static io.restassured.RestAssured.*;

import static org.hamcrest.Matchers.*;

import org.testng.annotations.Test;

public class TestingGetUsingGivenWhenApproch {
	
	  @Test
	  public void testStatusCode() {
	      
	      baseURI="https://reqres.in";
	      given().get("/api/users?page=2").then().statusCode(200);
	  }
	  @Test
	  public void testperticularValue()
	  {
	      baseURI="https://reqres.in";
	      given().get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));
	  }
	  @Test
	  public void printAllValues()
	  {
	      baseURI="https://reqres.in";
	      given().get("/api/users?page=2").then().log().all();
	  }
}

